<%-- 
    Document   : index
    Created on : 25-ene-2017, 9:52:37
    Author     : elcer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Operaciones en JSP</title>
        <link rel="stylesheet" href="css/cssIndex.css" type="text/css"></link>
        <script language="JavaScript" type="text/javascript" src="jss/jssIndex.js"></script>
    </head>
    <body>
        <%
            int n1=0, n2=0, suma=0, resta=0, prod=0, div=0, pot=0, rest=0;
            boolean err = false;
            if (request.getParameter("send") != null) {
                n1 = Integer.parseInt(request.getParameter("numero1"));
                n2 = Integer.parseInt(request.getParameter("numero2"));
                suma = n1+n2;
                resta = n1-n2;
                prod = n1*n2;
                
                pot = 1;
                int i = n2;
                while (i > 0) {
                    pot *= n1;
                    i--;
                }

                if (n2 != 0) {
                    div = n1/n2; 
                    rest = n1%n2;
                } else {
                    err = true;
                }
            }
        %>
        
        <h1>Tabla de Resultados</h1>
        <form action="index.jsp" nethod="POST">
            <table id="t1" border=0>
                <tr>
                    <td>Introduzca el primer operando...</td>
                    <td id="t1n1"><input type="text" name="numero1"></td>
                </tr>
                <tr>
                    <td>Introduzca el segundo operando...</td>
                    <td><input type="text" name="numero2"></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Enviar" name="send">
                    <input type="reset" value="Limpiar todo"></td>
                </tr>
                <br/>
            </table>
            
            
            <table id="t2">
                <tr>
                    <td>Suma (<% out.print(n1); %> + <% out.print(n2); %>):</td>
                    <td>
                        <input type="text" name="suma" value="
                               <% out.print(suma); %>
                        ">
                    </td>
                </tr>
                <tr>
                    <td>Resta (<% out.print(n1); %> - <% out.print(n2); %>):</td>
                    <td>
                        <input type="text" name="resta" value="
                               <% out.print(resta); %>
                        ">
                    </td>
                </tr>
                <tr>
                    <td>Multiplicaci&oacute;n (<% out.print(n1); %> * <% out.print(n2); %>):</td>
                    <td>
                        <input type="text" name="prod" value="
                               <% out.print(prod); %>
                        ">
                    </td>
                </tr>
                <tr>
                    <td>Divisi&oacute;n (<% out.print(n1); %> / <% out.print(n2); %>):</td>
                    <td>
                        <input type="text" name="div" value="
                               <% 
                                if (err) {
                                    out.println("No ha podido realizarse la operaci&oacute;n");
                                    out.print("Motivo: No se puede dividir por 0!!!");
                                } else {
                                    out.print(div);
                                }
                               %>
                        ">
                    </td>
                </tr>
                <tr>
                    <td>Potencia (<% out.print(n1); %> &#710; <% out.print(n2); %>)</td>
                    <td>
                        <input type="text" name="pot" value="
                               <% out.print(pot); %>
                        ">
                    </td>
                </tr>
                <tr>
                    <td>Resto (<% out.print(n1); %> % <% out.print(n2); %>):</td>
                    <td>
                        <input type="text" name="rest" value="
                               <% 
                                if (err) {
                                    out.println("No ha podido realizarse la operaci&oacute;n");
                                    out.print("Motivo: No se puede dividir por 0!!!");
                                } else {
                                    out.print(rest);
                                }
                               %>
                        ">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
